/*
        1. Опишіть своїми словами як працює метод forEach.
        forEach - это метод перебора массива, который выполняет указанную функцию один раз для каждого элемента.

        2. Як очистити масив?
        Простой способ отчистить массив с помощью свойства length, задать массиву length = 0.

        3. Як можна перевірити, що та чи інша змінна є масивом?
        Проверить можно с помощью метода .isArray(). Если значение массив, то результ будет true, если нет - false

*/

let arrItems = ['hello', 'world', 23, '23', null, 'function', 0, undefined, true];
const filterBy = (arr, type) => arr.filter(item => typeof item !== type)

    const newArr = filterBy(arrItems, 'string');
    console.log(arrItems);
    console.log(newArr);
