/*
    1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування?
    Экранирование это разделение специальных символов. Необходимо для поиска или отображения этих символов.

    2. Які засоби оголошення функцій ви знаєте?
    Declaration - с помощью ключевого слова funсtion и имени функции () скобок с параметрами или без и телом функции в {} скобках. Вызов функции может быть с любого места;
    Expression - может быть анонимной без имени функции. Вызов фунциии может быть только после нее;
    Стрелочная функция - используется синтаксис имя = () => {};

    3. Що таке hoisting, як він працює для змінних та функцій?
    Это "перемещение" переменных и функций вверх своей области видимости перед тем, как будет выполнен код.
*/

const createNewUser = () => {
    return newUser = {
        firstName: prompt( "Введите имя" ),
        lastName: prompt( "Введите Фамилию" ),
        birthDay: prompt( "Введите дату рождения в фомате ГГГГ-ММ-ДД" ),
        getAge() {
            let birthDayUser = Date.parse( this.birthDay );
            let result = Date.now() - birthDayUser;
            return Math.round(result / ( 365 * 24 * 3600 * 1000 ));
        },
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthDay;
        } 
    };
   
};
const user = createNewUser();
console.log( `Логин пользователя: ${user.getLogin()}` );
console.log( `Пароль пользователя: ${user.getPassword()}` );
console.log( `Возраст пользователя: ${user.getAge()} лет` );