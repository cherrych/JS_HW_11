/*
Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
Після натискання на кнопку сторінка не повинна перезавантажуватись
Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.
*/

const container = document.querySelector(".container-confirm");

const eyeOpen = document.querySelector("[data-eye=open]");
const eyeOpenConfirm = document.querySelector("[data-eye=open-confirm]");

const inputPass = document.querySelector(".pass");
const inputPassConfirm = document.querySelector(".pass-confirm");

const btn = document.querySelector(".btn");

eyeOpen.addEventListener("click", e => {
        if (inputPass.getAttribute('type') === 'password') {
            inputPass.setAttribute('type', 'text');
        } else {
            inputPass.setAttribute('type', 'password');
        }
    });

eyeOpenConfirm.addEventListener("click", e => {
        if (inputPassConfirm.getAttribute('type') === 'password') {
            inputPassConfirm.setAttribute('type', 'text');
        } else {
            inputPassConfirm.setAttribute('type', 'password');
        }
    });

const eyeFunc = () =>{
    addEventListener("click", e => {
                const value = e.target;
                value.classList.toggle("fa-eye-slash");
            });
};
eyeFunc(eyeOpen, eyeOpenConfirm);

//какая-то хрень: после нажатия на кнопку "Подтвердить" на ней появляется какой-то квадрат?
btn.addEventListener("click", e => {
        e.target = e.preventDefault();
        if(inputPass.value === inputPassConfirm.value) {
            alert("You are welcome");
        } else {
            const err = document.createElement("p");
            err.classList = "error";
            err.innerText = "Потрібно ввести однакові значення";
            container.appendChild(err);
            setTimeout(() => {
                err.innerText = "";
            },1500); 
        }
    });

    