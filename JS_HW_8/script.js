/*
        1. Опишіть своїми словами що таке Document Object Model (DOM)
 Это программа с помощью которой мы можем получить доступ к элементам HTML для редактирования, изменения структуры, оформления и т.д.

        2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
 innerText возвращает данные в виде строки, а innerHTML возвращает данные в виде текста с html тегами.


        3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
  .getElementById - обращение к элементу по id
  .querySelectorAll или .querySelector - лучше способ этот, потому, что можно обратиться к элементу по лубому селектору.

 */

//   Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const p = document.querySelectorAll('p');

p.forEach(item => item.style.backgroundColor = "#ff0000");
console.log(p)


//   Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const elemId = document.getElementById('optionsList');

console.log(elemId);
console.log(elemId.parentElement);
console.log(elemId.childNodes); 
//  ВОПРОС? Почему у меня не получается вывести в консоль таким образом: 
// console.log(`Это нода дочерних элементов: ${elemId.childNodes}`); 
// на выходе консоль выдает - Это нода дочерних элементов: [object NodeList]


//   Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
const addPar = document.getElementById('testParagraph');

addPar.textContent = 'This is a paragraph';
console.log(addPar);

  
//   Отримати елементи , вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const getElement = () => {
       const classEement = document.querySelector('.main-header');
       console.log(classEement);
       const allElemInClass = classEement.querySelectorAll('*');
       console.log(allElemInClass);
       allElemInClass.forEach(item => item.classList = 'nav-item');
       // [...allElemInClass].forEach(item => item.classList = 'nav-item'); 
       // ВОПРОС? В чем разница строки №46 и строки №47 в консоли вывод одинаковый
};
getElement();


//   Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const delClassElement = document.querySelectorAll('.section-title');
       console.log(delClassElement);
       delClassElement.forEach(item => item.classList.remove('section-title'));
       console.log(delClassElement);




