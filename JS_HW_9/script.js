/*
        1. Опишіть, як можна створити новий HTML тег на сторінці.
С помощью метода document.createElement(), а также с помощью  document.insertAdjacentHTML(),
document.createTextNode() – создаёт текстовый узел, я так понимаю, что с помощью .cloneNode() тоже можно создать, точнее скопировать узел. Также есть document.write() - это походу старая штука уже не используется. 

        2.Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
Первый параметр это место куда будет производиться вставка по отношению к элементу. Может быть 
beforebegin - до начала элемента, afterbegin - после начала, beforeend - до закрытия элемента, afterend - после элемента. А второй параметр это HTML строка, которая будет вставлена как HTML.

        3.Як можна видалити елемент зі сторінки?
C помощью метода .remove() 
*/

// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body).
//     кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function getArr(arr, element = document.body) {

    const newUl = document.createElement('ul');
    newUl.className = 'item-list';
       
    const allItem = arr.map(item => {
        const newLi = document.createElement('li');
        newLi.classList.add('item');
        newLi.textContent = item;
        return newLi;
    });

    newUl.append(...allItem);
    element.append(newUl);
};
getArr(arr);